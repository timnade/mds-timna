package com.example;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MyDataStructure {
	Map<String,Pair> map;
	Timer timer;

	public MyDataStructure(int capacity) {
		map = Collections.synchronizedMap(
				new LinkedHashMap<String,Pair>(capacity, 0.75F, false) {
					protected boolean removeEldestEntry(Map.Entry<String,Pair> eldest) {
						return size() > capacity;
	         }
	      });
		 timer = new Timer();
	}
	
	/**
	 * - add value to the data structure. complexity is o(1)
	 * - first check if the key exists in the Map and remove it(if necessary)
	 *   this is done to maintain insertion order if a key is re-inserted into the map
	 *   and make sure time-to-live will reboot as well
	 * - remove item after the time-to-live has elapsed 
	 *   (unless time-to-live is 0 and then the item does not expire)
	 * @param key
	 * @param value
	 * @param timeToLive
	 */
	public void put(String key, Object value, int timeToLive) {
		synchronized(map){
			remove(key);
			//create a TimerTask that will remove the item after the time-to-live has elapsed
			TimerTask task=null;
			if (timeToLive>0) {
				 task = new java.util.TimerTask() {
			            @Override
			            public void run() {
			            	remove(key);
			            }
			        };
				timer.schedule(task,timeToLive);
			}
			map.put(key, new Pair(value,task));	
		}
	}

	/**
	 * remove value from the data structure. complexity is o(1)
	 * @param key
	 */
	public void remove(String key) {
		synchronized(map){
			if(map.containsKey(key)){
				//check if needed to cancel a task, in case the same key will be inserted again 
				TimerTask task=map.get(key).getTask();
				if (task!=null)
					task.cancel();
				map.remove(key);
			}
		}
	}

	/**
	 * get value from the data structure. complexity is o(1)
	 * @param key
	 * @return
	 */
	public Object get(String key) {
		synchronized(map){
			Pair pair = map.get(key);
			if (pair!=null)
				return pair.getValue();
			return null;
		}
	}

	/**
	 * get number of keys in the data structure. complexity is o(1)
	 * @return number of keys in the data structure
	 */
	public int size() {
		return map.size();
	}
	
	
	private class Pair {
		private final Object value;
		private final TimerTask task;

		private Pair(Object value, TimerTask task) {
			this.value = value;
			this.task = task;
		}

		private Object getValue() { return value; }
		private TimerTask getTask() { return task; }
	}
}


